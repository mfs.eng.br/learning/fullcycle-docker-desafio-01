FROM  golang:latest as builder

WORKDIR /app
COPY ./* /app
RUN go build main.go


FROM scratch
WORKDIR /app
COPY --from=builder /app/* /app

CMD ["/app/main"]



